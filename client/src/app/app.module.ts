import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { MaterialModule } from './shared/material.module';

import { AuthenticationService } from './service/auth.service';
import { SharedDataService } from './service/shared-data.service';
import { AuthGuard } from './service/auth.guard';
import {ChatService} from './service/chat.service';

import { AppRoutingModule } from './app-routing.module';
import { AppCommonModule } from './common/common.module';

import { Configuration } from './config/configuration';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SignComponent } from './components/sign/sign.component';
import { HomeComponent } from './components/home/home.component';
import {ChangePassComponent} from './components/changepass/changepass.component'
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignComponent,
    HomeComponent,
    ChangePassComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpModule,
    AppCommonModule,
  ],
  exports: [
    AppCommonModule
  ],
  providers: [
    AuthenticationService,
    AuthGuard,
    Configuration,
    SharedDataService,
    ChatService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
