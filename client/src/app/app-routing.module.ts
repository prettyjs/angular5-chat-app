import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { SignComponent } from './components/sign/sign.component';
import { HomeComponent } from './components/home/home.component';
import {ChangePassComponent} from './components/changepass/changepass.component'
import { AuthGuard } from './service/auth.guard';

const appRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: LoginComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'signup',
        canActivate: [AuthGuard],
        component: SignComponent,
      }
    ]
  },
  {
    path:"",
    children:[
      {
        path: 'home',
        canActivate: [AuthGuard],
        component: HomeComponent,
      },
      {
        path: 'changepass',
        canActivate: [AuthGuard],
        component: ChangePassComponent,
      }
    ]
  },
  { path: '**', redirectTo: '' }
];


@NgModule({
  exports: [
    RouterModule
  ],
  imports: [
    CommonModule,
    // RouterModule.forRoot(appRoutes, { enableTracing: true })
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthGuard,
  ],
  schemas: [
    NO_ERRORS_SCHEMA,
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppRoutingModule { }
