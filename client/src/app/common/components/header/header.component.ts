import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthenticationService } from '../../../service/auth.service';
import { SharedDataService } from '../../../service/shared-data.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private sharedData: SharedDataService,
    ) {}

    ngOnInit() {
    }

    public logOut() {
        this.sharedData.clearSession();
        this.router.navigate(['']);
    }

    public changePass(){
        this.router.navigate(['changepass']);        
    }
}
