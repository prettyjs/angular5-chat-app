import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { ChangePassModel } from '../../models/base.model.data';
import { AuthenticationService } from '../../service/auth.service';
import { ValidationService } from '../../service/validation.service';
import { SharedDataService } from '../../service/shared-data.service';

declare interface ValidatorFn {
  (c: AbstractControl): {
    [key: string]: any;
  };
}

@NgModule({
  imports: [
  ],
  declarations: [
  ]
})

@Component({
  selector: 'app-change-pass',
  templateUrl: './changepass.component.html',
  styleUrls: ['./changepass.component.css']
})

export class ChangePassComponent implements OnInit {
  changePassForm: FormGroup;
  model: ChangePassModel;
  cuser: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthenticationService,
    private sharedData: SharedDataService
  ) {
    this.changePassForm = new FormGroup({
      oldPassword: new FormControl('', Validators.compose([Validators.required, ValidationService.allBlankSpaces])),
      newPassword: new FormControl('', Validators.compose([Validators.required, ValidationService.customerNameValidatorSpecialCharacters6letters]))
    });
    this.cuser = sharedData.getSessionData()
  }

  onChangePass() {
    if (this.changePassForm.valid) {
      this.model = {
        userID : this.cuser.user._id,
        oldpass : this.changePassForm.value.oldPassword,
        newpass : this.changePassForm.value.newPassword
      }     
      this.auth.changePassword(this.model)
        .subscribe(res => {
          if (!res.error && this.sharedData) {
            this.sharedData.clearSession();
            this.router.navigate(['login']);
          }
        })
    }
  }
  ngOnInit() {
    if (!this.auth.isSignIn()) {
      this.router.navigate(['login'])
    }
  }
  onCancel(){
    this.router.navigate(['home']);
  }
}
