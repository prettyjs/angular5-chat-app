import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { AuthenticationModel } from '../../models/base.model.data';
import { AuthenticationService } from '../../service/auth.service';
import { ValidationService } from '../../service/validation.service';
import { SharedDataService } from '../../service/shared-data.service';

declare interface ValidatorFn {
  (c: AbstractControl): {
    [key: string]: any;
  };
}

@NgModule({
  imports: [
  ],
  declarations: [
  ]
})

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  model: AuthenticationModel;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthenticationService,
    private sharedData: SharedDataService
  ) {
    this.loginForm = new FormGroup({
      useremail: new FormControl('', Validators.compose([Validators.required, ValidationService.emailValidator])),
      password: new FormControl('', Validators.compose([Validators.required, ValidationService.allBlankSpaces]))
    });
  }

  onRegister() {
    this.router.navigate(['signup']);
  }
  onLogin() {
    if (this.loginForm.valid) {
      this.model = {
        email: this.loginForm.value.useremail,
        password: this.loginForm.value.password
      }
      this.auth.AuthenticateUser(this.model)
        .subscribe(res => {
          if (!res.error && this.sharedData) {
            this.sharedData.storeSessionData(res);
            this.router.navigate(['home']);
          }
        })
    }
  }
  ngOnInit() {
    if (this.auth.isSignIn()) {
      this.router.navigate(['home'])
    }
  }

}
