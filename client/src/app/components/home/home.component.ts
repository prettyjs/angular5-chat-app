import { Component, OnInit } from '@angular/core';
import { MessageModel } from '../../models/base.model.data';
import { ChatService } from '../../service/chat.service';
import { SharedDataService } from '../../service/shared-data.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  inpuntMsg: string;
  cuser: any;
  message: MessageModel;
  messages: Array<MessageModel>;
  constructor(private chatService: ChatService, private sharedData: SharedDataService) {
    this.chatService.allMessages();
    this.chatService.getMessages()
      .subscribe((messages: Array<MessageModel>) => {
        this.messages = messages;
      })
    this.cuser = sharedData.getSessionData()
  }

  ngOnInit() {
    this.chatService
      .getMessage()
      .subscribe((message: any) => {
        this.messages.push(message);
      });
  }

  isMe(userID) {
    return userID === this.cuser.user._id ? "rightside-right-chat" : "rightside-left-chat";
  }

  getAvatar(userID){
    return userID === this.cuser.user._id ? 
    "../../../assets/img/faces/avatar.jpg" 
    : "../../../assets/img/faces/card-profile1-square.jpg";    
  }

  sendMessage() {
    if (!this.inpuntMsg) return;
    this.message = {
      message: this.inpuntMsg,
      userID: this.cuser.user._id,
      userName: this.cuser.user.userName
    }
    this.chatService.sendMessage(this.message);
    this.inpuntMsg = null;
  }
  scrollToBottom() {
    // setTimeout(function () {
      document.getElementById('chatboard').scrollBy(0, 5000)
    // }, 100)
  }
  onKey(event: any) {
    if (event.charCode === 13) this.sendMessage();
  }
}
