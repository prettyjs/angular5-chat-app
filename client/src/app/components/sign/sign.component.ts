import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { SignModel } from '../../models/base.model.data';
import { AuthenticationService } from '../../service/auth.service';
import { ValidationService } from '../../service/validation.service';
import { SharedDataService } from '../../service/shared-data.service';


@Component({
  selector: 'app-sign',
  templateUrl: './sign.component.html',
  styleUrls: ['./sign.component.css']
})
export class SignComponent implements OnInit {

  signForm: FormGroup;
  model: SignModel;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthenticationService,
    private sharedData: SharedDataService
  ) {
    this.signForm = new FormGroup({
      username: new FormControl('', Validators.compose([Validators.required])),
      email: new FormControl('', Validators.compose([Validators.required, ValidationService.emailValidator])),
      password: new FormControl('', Validators.compose([Validators.required])),
      confirmpassword: new FormControl('', Validators.compose([Validators.required]))
    })
  }

  onLogin() {
    this.router.navigate(['login']);
  }

  onRegister() {
    if (this.signForm.valid) {
      this.model = {
        username: this.signForm.value.username,
        email: this.signForm.value.email,
        password: this.signForm.value.password,
        confirmpassword: this.signForm.value.confirmpassword
      }
      this.auth.SignupUser(this.model)
        .subscribe(res => {
          if (!res.error) this.router.navigate(['login']);
        })
    }
  }

  ngOnInit() {
    if (this.auth.isSignIn()) {
      //some router here
      // this.router.navigate(['sign'])
    }
  }

}
