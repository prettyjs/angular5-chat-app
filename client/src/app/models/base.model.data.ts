export class AuthenticationModel {
    email: string;
    password: string;
}

export class SignModel {
    username: string;
    email: string;
    password: string;
    confirmpassword: string;
}

export class MessageModel {
    message: string
    userID: string
    userName: string
}

export class ChangePassModel {
    userID: string
    oldpass: string
    newpass: string
}
