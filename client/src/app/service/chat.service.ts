import * as io from 'socket.io-client';
import { Observable } from 'rxjs/Observable';

export class ChatService {
  private url = 'http://localhost:3030';
  private socket;

  constructor() {
    this.socket = io(this.url);
  }

  public sendMessage(message) {
    this.socket.emit('SEND_MESSAGE', message);
  }

  public getMessage = () => {
    return Observable.create((observer) => {
      this.socket.on('RECEIVE_MESSAGE', (message) => {
        observer.next(message);
      });
    });
  }

  public allMessages() {
    this.socket.emit('GET_ALL_MESSAGES');
  }

  public getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('RECEIVE_ALL_MESSAGE', (messages) => {
        observer.next(messages);
      });
    });
  }

}