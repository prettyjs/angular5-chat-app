const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const { MongoClient } = require('mongodb');
const cors = require('cors')

const appConfig = require('./config/config');

const port = appConfig.port;
const app = express();

const start = async () => {
    try {

        const db = await MongoClient.connect(appConfig.mongoUrl);
        app.use(bodyParser.json());
        app.use(cors())
        const server = app.listen(port, () => { console.log(`💝  server listening on port ${port}`) });

        require('./server/route/users')(app, db, appConfig);
        require('./server/socket/soket')(server, db);
        require('./server/route/workspace')(app, db);
        require('./server/route/upload')(app);

    } catch (e) {
        console.log(e)
    }
};
start();

